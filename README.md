# Sass 심화

## 벤치마킹중..

<!--  경희님 -->
NAVER Developers  
<https://developers.naver.com/main/>

KaKao Developers  
<https://developers.kakao.com/>

11st OPEN API Center  
<http://openapi.11st.co.kr/openapi/OpenApiFrontMain.tmall>

Google Play 개발자 정책 센터  
<https://play.google.com/about/developer-content-policy/>

Windows 개발자 센터  
<https://developer.microsoft.com/ko-kr/windows>

Was 개발자센터  
<https://aws.amazon.com/ko/developer/>

NHBank  
<https://banking.nonghyup.com/nhbank.html>

NH Developers (모바일 화면은 없음)  
<https://developers.nonghyup.com/>

리니지(LINEAGE)  
<https://lineage2m.plaync.com/guidebook/view#title=%ED%80%98%EC%8A%A4%ED%8A%B8>

<!--  은선님 -->
다음  
<https://daumui.tistory.com/>

W3Schools  
<https://www.w3schools.com/>

<!--  승찬님 -->
Material Design  
<https://material.io/components/>

Material-UI  
<https://material-ui.com/getting-started/installation/>

Angular Material  
<https://material.angular.io/components/categories>

Styled Components  
<https://www.styled-components.com/docs/basics#installation>

React Bootstrap  
<https://react-bootstrap.github.io/getting-started/introduction/>
